@extends('dash.layout')
@section('content')
  <div id="banner-setting">
    <h3>Pengaturan Banner <span class="text-danger">{{ __('banner.' . $type) }}</span></h3>

    <a href="{{ route('dash.banner.add', $type) }}" class="btn btn-danger btn-sm tambah-banner-button mb-3">Tambah
      Banner</a>

    <table class="table table-striped table-hover table-responsive">
      <thead>
        <tr>

          <th class="text-center">IMG</th>
          {{-- <th class="text-center">WIDTH</th>
          <th class="text-center">HEIGHT</th>
          <th class="text-center">CLASS</th>
          <th class="text-center">LINK</th>
          <th class="text-center">ALT</th>
          <th class="text-center">TITLE</th>
          <th class="text-center">REL</th>
          <th class="text-center">OPT</th> --}}
          <th class="text-center">ACTION</th>
        </tr>
      </thead>
      <tbody id="sortable" data-type="{{ $type }}">
        @foreach ($list as $banner)
          <tr id="{{ $banner->id }}">

            <td class="text-center"><a href="{{ $banner->link }}" target="_blank" tittle="{{ $banner->title }}"><img
                  src="{{ $banner->img }}" onerror="this.onerror=null;this.src='https://placeimg.com/200/300/animals';"
                  alt="{{ $banner->alt }}" class="{{ $banner->class }}" height="{{ $banner->height }}"
                  width="{{ $banner->width }}" loading="lazy"></a></td>
            {{-- <td class="text-center">{{ $banner->width }}px</td>
            <td class="text-center">{{ $banner->height }}px</td>
            <td class="text-center">{{ $banner->class }}</td>
            <td class=""><a href="{{ $banner->link }}" title="{{ $banner->link }}"
                class="btn btn-sm btn-block btn-success ">click</a></td>
            <td class="">{{ $banner->alt }}</td>
            <td class="">{{ $banner->title }}</td>
            <td class="">{{ $banner->rel }}</td>
            <td class="">{{ $banner->opt }}</td> --}}
            <td class="text-center">
              <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                <a href="{{ route('dash.banner.edit', ['bannertype' => $type, 'bannerid' => $banner->id]) }}"
                  class="btn btn-warning ">Edit</a>

                <button type="button" class="btn btn-danger btn-delete-banner"
                  data-id="{{ $banner->id }}">Delete</button>
              </div>
            </td>

          </tr>

        @endforeach
      </tbody>
    </table>


  </div>
@endsection


@push('css')

  <link rel="stylesheet" href="/assets/plugins/izitoast/css/iziToast.min.css">
@endpush
@push('js')

  <script src="/assets/plugins/izitoast/js/iziToast.min.js"></script>



@endpush
