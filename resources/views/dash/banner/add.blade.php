@extends('dash.layout')
@section('content')
  <div id="banner-setting">
    <h3>Add Banner <span>{{ __('banner.' . $type) }}</span></h3>

    <form action="{{ route('dash.banner.add.action', $type) }}" class="row" method="POST">
      @csrf

      @if ($action === 'edit')
        <input type="hidden" name="id" value="{{ $data->id }}">
      @endif
      <input type="hidden" name="action" value="{{ $action }}">
      <input type="hidden" class="form-control" placeholder="" name="type" id="type" value="{{ $type }}">

      <div class="mb-1">
        <label for="img" class="form-label">Link Banner </label>
        <input type="text" class="form-control" placeholder="" name="img" id="img" required
          value="{{ isset($data) ? $data->img : '' }}" autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="width" class="form-label">Banner Width</label>
        <input type="number" class="form-control" placeholder="width " name="width"
          value="{{ isset($data) ? $data->width : '' }}" id="width" required autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="height" class="form-label">Banner Height</label>
        <input type="number" class="form-control" placeholder="height " name="height" id="height"
          value="{{ isset($data) ? $data->height : '' }}" required autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="class" class="form-label">Banner Class</label>
        <input type="text" class="form-control" placeholder="class " name="class" id="class"
          value="{{ isset($data) ? $data->class : '' }}" autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="link" class="form-label">Banner Link Href</label>
        <input type="text" class="form-control" placeholder="link " name="link" id="link"
          value="{{ isset($data) ? $data->link : '' }}" required autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="alt" class="form-label">Banner Link Alt </label>
        <input type="text" class="form-control" placeholder="alt " name="alt" id="alt"
          value="{{ isset($data) ? $data->alt : '' }}" required autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="title" class="form-label">Banner Link Title</label>
        <input type="text" class="form-control" placeholder="title " name="title" id="title"
          value="{{ isset($data) ? $data->title : '' }}" required autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="rel" class="form-label">Banner Link Rel</label>
        <input type="text" class="form-control" placeholder="rel " name="rel" id="rel"
          value="{{ isset($data) ? $data->rel : '' }}" required autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="opt" class="form-label">Optional</label>
        <input type="text" class="form-control" placeholder="opt " name="opt" id="opt"
          value="{{ isset($data) ? $data->opt : '' }}" autocomplete="off">
      </div>

      <div class="d-grid gap-2 mt-3">
        <input type="submit" class="btn  btn-success" value="ADD BANNER">

      </div>



    </form>


  </div>
@endsection
