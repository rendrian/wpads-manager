@extends('auth.layout')
@section('content')

  <form action="{{ route('do.login') }}" method="POST">
    @csrf
    <input type="text" name="email" placeholder="email" autocomplete="off">
    <input type="password" name="password" placeholder="password" autocomplete="off">
    <input type="submit" value="LOGIN">

  </form>
@endsection
