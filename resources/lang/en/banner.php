<?php

return [
    "majalahpro_core_top_banner" => "Top Banner",
    "majalahpro_core_top_banner_after_menu" => "After Menu",
    "majalahpro_core_add_banner_inside_content" => "Inside Content",
    "majalahpro_core_add_banner_inside_content_other" => "Inside Content Other",
    "majalahpro_core_banner_before_content" => "Before Content",
    "majalahpro_core_banner_after_content" => "After Content",
    "majalahpro_core_floating_banner_footer" => "Floating Footer",
    "majalahpro_core_floating_banner_left" => "Floating Left",
    "majalahpro_core_floating_banner_right" => "Floating Right",
    "majalahpro_core_floating_banner_popup_1" => "Popup Banner 1",
    "majalahpro_core_floating_banner_popup_2" => "Popup Banner 2",
    "majalahpro_core_floating_banner_popup_3" => "Popup Banner 3",
    "majalahpro_core_floating_banner_popup_4" => "Popup Banner 4",
    "majalahpro_core_banner_after_sidebar" => "Sidebar Banner",

];
