require('./bootstrap');




$(function() {

    var tableSortOptions = {
        update: function(event, ui) {

            var position = $("#sortable").sortable("toArray");
            var type = $("#sortable").data("type")

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var formData = {
                data: position,
                type: type
            }
            var ajaxurl = '/api/save-banner-position';
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        iziToast.success({
                            // title: 'Hello, world!',
                            message: 'Position Updated',
                            position: 'topCenter'
                        });
                    }

                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    var tableSortOptionsLink = {
        update: function(event, ui) {

            var position = $("#sortable-link").sortable("toArray");


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var formData = {
                data: position,

            }
            var ajaxurl = '/api/save-link-position';
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        iziToast.success({
                            // title: 'Hello, world!',
                            message: 'Position Updated',
                            position: 'topCenter'
                        });
                    }

                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    $("#sortable").sortable(tableSortOptions);
    $("#sortable-link").sortable(tableSortOptionsLink);



    $(".btn-delete-banner").click(function() {
        var iddata = $(this).data('id');
        iziToast.show({
            theme: 'dark',
            icon: 'icon-person',
            title: 'Hey',
            message: 'Delete Data ? ',
            position: 'center', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
            progressBarColor: 'rgb(0, 255, 184)',
            buttons: [
                ['<button>Ok</button>', function(instance, toast) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var formData = {
                        id: iddata,

                    }
                    var ajaxurl = '/api/deleteBannerConfirmation';
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        data: formData,
                        dataType: 'json',
                        success: function(data) {
                            if (data.success == true) {
                                iziToast.success({
                                    // title: 'Hello, world!',
                                    message: 'Position Updated',
                                    position: 'topCenter'
                                });
                                location.reload()
                            }

                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                }, true], // true to focus
                ['<button>Close</button>', function(instance, toast) {
                    instance.hide({
                        transitionOut: 'fadeOutUp',
                        onClosing: function(instance, toast, closedBy) {

                        }
                    }, toast, 'buttonName');
                }]
            ],

        });
    });
    $(".jur-delete").click(function() {
        var iddata = $(this).data('id');
        iziToast.show({
            theme: 'dark',
            icon: 'icon-person',
            title: 'Hey',
            message: 'Delete Data ? ',
            position: 'center', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
            progressBarColor: 'rgb(0, 255, 184)',
            buttons: [
                ['<button>Ok</button>', function(instance, toast) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var formData = {
                        id: iddata,

                    }
                    var ajaxurl = '/api/deleteJudiRekomendasi';
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        data: formData,
                        dataType: 'json',
                        success: function(data) {
                            if (data.success == true) {
                                iziToast.success({
                                    // title: 'Hello, world!',
                                    message: 'Position Updated',
                                    position: 'topCenter'
                                });
                                location.reload()
                            }

                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                }, true], // true to focus
                ['<button>Close</button>', function(instance, toast) {
                    instance.hide({
                        transitionOut: 'fadeOutUp',
                        onClosing: function(instance, toast, closedBy) {

                        }
                    }, toast, 'buttonName');
                }]
            ],

        });
    });
});