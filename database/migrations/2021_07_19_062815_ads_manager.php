<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdsManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_list', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('type');
            $table->string('img')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->string('class')->nullable();
            $table->string('link')->nullable();
            $table->string('alt')->nullable();
            $table->string('title')->nullable();
            $table->string('rel')->nullable();
            $table->string('opt')->nullable();
            $table->integer('position')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_list');
    }
}
