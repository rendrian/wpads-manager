<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Banner;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fill = [];
        $counter  = 1;
        foreach (sidebarBannerMenu() as   $value) {
            $exist = Banner::where('type', $value)->first();
            if (!$exist) {
                for ($i = 0; $i <  2; $i++) {
                    $newBanner =  new Banner(['id' => Str::uuid()]);
                    $newBanner->fill([
                        'type' => $value,
                        "img" => "https://1.bp.blogspot.com/-GJC-K7DcR4o/X9zF6Evb4SI/AAAAAAAAM04/lX1K5CeGqlkY5AOc7K_5RoqVpSjIwuW_wCLcBGAsYHQ/s0/QQ%2BSlot.gif",
                        "width" => "570",
                        "height" => "70",
                        "class" => "class",
                        "link" => "https://google.com",
                        "alt" => "iklan" . $counter,
                        "title" => "iklan" . $counter,
                        "rel" => "sponsored noopener noreferrer",
                        "opt" => "iklan" . $counter
                    ]);
                    $newBanner->save();
                }
                $counter++;
            }
        }
    }
}
