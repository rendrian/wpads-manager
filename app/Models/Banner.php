<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $table = 'ads_list';
    protected $fillable = [
        "id",
        "type",
        "img",
        "width",
        "height",
        "class",
        "link",
        "alt",
        "title",
        "rel",
        "opt",
        'position',

    ];
    protected $primaryKey = 'id'; // or null

    public $incrementing = false;
}
