<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\JudiRekomendasi;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function saveLinkPosition(Request $request)
    {

        try {
            foreach ($request->data as $key => $bannerId) {
                JudiRekomendasi::where('id', $bannerId)->update(['position' => $key + 1]);
            }
            saveLinkRekomendasi();
            return response([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
    public function saveBannerPosition(Request $request)
    {

        try {
            foreach ($request->data as $key => $bannerId) {
                Banner::where('id', $bannerId)->update(['position' => $key + 1]);
            }
            return response([
                'success' => true,

            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
    public function getAllBanner()
    {
        $data = Banner::orderBy('position', 'asc')->get()->groupBy('type');
        // $data =   Cache::remember('allBanner', 300, function () {
        //     return 
        // });

        return response([
            'success' => true,
            'data' => $data
        ]);
    }

    public function  deleteBannerConfirmation(Request $request)
    {
        $id = $request->id;
        try {
            Banner::destroy($id);
            return response([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            return response([
                'success' => true,
                'msg' => $th->getMessage()
            ]);
        }
    }
    public function  deleteJudiRekomendasi(Request $request)
    {
        $id = $request->id;
        try {
            JudiRekomendasi::destroy($id);
            saveLinkRekomendasi();
            return response([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            return response([
                'success' => true,
                'msg' => $th->getMessage()
            ]);
        }
    }
}
