<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DashController extends Controller
{
    public function index()
    {
        return view('dash.index');
    }
    public function banner($type)
    {
        $list = getBannerByType($type);

        return view('dash.banner.list', [
            'type' => $type,
            'list' => $list
        ]);
    }

    public function bannerAdd($type)
    {
        return view('dash.banner.add', [
            'type' => $type,
            'action' => 'addnew'
        ]);
    }



    public function bannerAddAction($type, Request $request)
    {
        if ($request->action == 'addnew') {
            $input = $request->except('_token', 'action');
            $data = new Banner(['id' => Str::uuid()]);
            $data->fill($input);
            $save = $data->save();
            if ($save) {
                return redirect(route('dash.banner', $type));
            }
        } else {
            // return $request;
            try {
                //code...
                $update  = Banner::find($request->id);
                $update->update($request->except('_token', 'action'));
                return redirect(route('dash.banner', $type));
            } catch (\Throwable $th) {
                throw $th;
            }


            // $save = $data->save();
            // if ($save) {
            //     return redirect(route('dash.banner', $type));
            // }
        }
    }


    public function bannerEdit($type, $id)
    {
        $data = Banner::find($id);

        return view('dash.banner.add', [
            'type' => $type,
            'action' => 'edit',
            'data' => $data
        ]);
    }
}
