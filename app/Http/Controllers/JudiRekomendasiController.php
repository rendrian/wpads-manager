<?php

namespace App\Http\Controllers;

use App\Models\JudiRekomendasi;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class JudiRekomendasiController extends Controller
{
    public function index()
    {
        $data = JudiRekomendasi::orderBy('position', 'asc')->get();
        return view('judirekomendasi.index', ['data' => $data]);
    }
    public function edit($id)
    {

        $data = JudiRekomendasi::find($id);
        return view('judirekomendasi.add', ['action' => 'edit', 'data' => $data]);
    }

    public function add()
    {
        return view('judirekomendasi.add', ['action' => 'add']);
    }
    public function doAdd(Request $request)
    {
        if ($request->action === 'add') {
            $lastPosition = JudiRekomendasi::max('position');
            if ($lastPosition === null) {
                $lastPosition = 1;
            } else {
                $lastPosition = $lastPosition + 1;
            }


            $fill = $request->except('_token', 'action');
            $data = new JudiRekomendasi(['id' => Str::uuid()]);
            $data->fill($fill);
            $data->position = $lastPosition;

            $data->save();
            saveLinkRekomendasi();


            if ($data) {
                return redirect(route('judi_rekomendasi.index'));
            }
        } else {
            $data = JudiRekomendasi::find($request->id);
            $data->name = $request->name;
            $data->img = $request->img;
            $data->url = $request->url;
            $data->save();
            saveLinkRekomendasi();
            if ($data) {
                return redirect(route('judi_rekomendasi.index'));
            }
        }
    }
}
